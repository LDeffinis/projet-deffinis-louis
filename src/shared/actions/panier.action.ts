import { Article } from '../models/article';

export class AddArticle {
  static readonly type = "[Article] Add";

  constructor(public payload: Article) {}
}

export class RemoveArticle {
  static readonly type = "[Article] Remove";

  // constructor(public payload: Article) {}
  constructor(public payload: string) {}
}

export class RemoveAllArticles {
  
  static readonly type = "[Article] RemoveAll";

  // constructor(public payload: Article) {}
  constructor() {
    console.log('RemoveAllArticles');
  }
  //constructor(public payload: string) {}
}
