export interface Article {
  ref: string;
  titre: string;
  prix: number;
}
