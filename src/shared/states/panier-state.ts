import { Injectable } from '@angular/core';
import { Action, State, StateContext, Selector } from '@ngxs/store'
import { AddArticle, RemoveArticle, RemoveAllArticles } from '../actions/panier.action'
import { Article } from '../models/article';
import { PanierStateModel } from './article-panier-model';

@State<PanierStateModel>(
  {
    name: 'panier',
    defaults: {
      panier: []
    }
  }
)

@Injectable()
export class PanierState {

  @Selector()
  static getArticles(state: PanierStateModel) {
    return state.panier;
  }

  @Selector()
  static getNbArticles(state: PanierStateModel) {
    return state.panier.length;
  }

  @Selector()
  static getPanierPrix(state: PanierStateModel) {

    let prix=0;

    state.panier.forEach(function(item){
      prix = prix + item.prix;
    });

    return prix;
  }

  @Action(AddArticle)
  add(
    { getState, patchState }: StateContext<PanierStateModel>,
    { payload }: AddArticle) {
    const state = getState();
    console.log(payload);
    patchState({
      panier: [...state.panier, payload]
    });
  }
  
  @Action(RemoveArticle)
  del(
    { getState, patchState }: StateContext<PanierStateModel>,
    { payload }: RemoveArticle) {
    patchState({
      panier: getState().panier.filter(x => x.ref != payload)
    });
  }

  @Action(RemoveAllArticles)
  delAll(
    
    { getState, patchState }: StateContext<PanierStateModel>,
    { }: RemoveAllArticles) {
    patchState({ 
      panier: getState().panier.filter(x => 0)
    });

    console.log("DellAll");
  }

}
