import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private api:ApiService) { }

  ngOnInit(): void {
  }

  prenom: string = "";
  nom: string = "";

  regEmpty = /^(?!\s*$).+/;
  regAlpha = /[A-Za-z]{2,30}/;

  onSubmit(data:any){
    console.log(data);

    this.api.postLogin(data).subscribe((result => {
      console.log(result);
    }));
    
  }

}
