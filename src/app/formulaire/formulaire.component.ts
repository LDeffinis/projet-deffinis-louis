import { Component, OnInit, Input, Injectable } from '@angular/core';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css'],
})
export class FormulaireComponent implements OnInit {

  constructor( private api:ApiService) { }

  prenom: string = "";
  nom: string = "";
  ville: string = "";
  codePostal: string = "";
  adresse: string = "";
  error: boolean = true;

  regEmpty = /^(?!\s*$).+/;
  regAlpha = /[A-Za-z]{2,30}/;
  regAlphaNum = /[A-Za-z0-9]{2,30}/;
  regNum = /^[0-9]{5}$/;

  @Input() erreur: boolean = true;

  ngOnInit(): void {
  }

  onSubmit(data:any){
    console.log(data);

    this.api.postFormulaire(data).subscribe((result => {
      console.log(result);
    }));
    
  }
}
