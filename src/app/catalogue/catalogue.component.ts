import { Component, Input, OnInit } from '@angular/core';
import { Observable, of, from, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { serializeNodes } from '@angular/compiler/src/i18n/digest';
import { FormControl } from '@angular/forms';
import { ArticleService } from '../service/article.service';
import { Article } from 'src/shared/models/article';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})

export class CatalogueComponent implements OnInit {

  articles$!: Article[];

  articleSubscription!: Subscription;

  constructor(private articleService: ArticleService, private api:ApiService) { }

  ngOnInit(): void {
    
    /*
    this.articleSubscription = this.articleService.articleSubject.subscribe(
      (articles: Article[]) => {
        this.articles$! = articles;
      }
    );
    this.articleService.emitArticleSubject();
    */
    /*
    this.api.getCatalogue().subscribe((result => {
      console.log(result);
    }));
    */

    this.articleSubscription = this.api.getCatalogue().subscribe(
      (articles: Article[]) => {
        this.articles$! = articles;
      }
    );
    this.articleService.emitArticleSubject();

  }
}
