import { RemoveArticle } from './../../shared/actions/panier.action';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Article } from 'src/shared/models/article';
import { PanierState } from 'src/shared/states/panier-state';
import { ArticleService } from '../service/article.service';
import { RemoveAllArticles } from '../../shared/actions/panier.action';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  nbElementPanier: any;

  panierElements$!: Observable<Article[]>;

  panierPrix : any;

  constructor(private articleService: ArticleService,
    private store: Store) { }

  ngOnInit(): void {
    this.panierElements$ = this.store.select(state => state.panier.panier);

    let total =0;
    this.panierElements$.forEach(function(element){
      element.forEach(function(value){
        total = total + value.prix;
      });
    });

    this.panierPrix = total;
  }

  viderPanier(){
    console.log('viderPanier');
    this.store.dispatch(new RemoveAllArticles());
  }

}
