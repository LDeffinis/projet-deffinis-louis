import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})

export class ApiService {
  constructor(private httpClient : HttpClient) { 
    this.datas = new Array<string> ();  
  }

  cpt : number = 0;
  datas : string [];

  log(data: string) {
    this.datas.push(data);
    this.cpt++;
  }

  public getCatalogue () : Observable<any> {
    return this.httpClient.get<any> (environment.baseUrl+'/api/catalogue');
  }

  public postFormulaire(data : any) : Observable<any>{
    return this.httpClient.post(environment.baseUrl+'/api/formulaire',data);
  }

  public postLogin(data : any) : Observable<any>{
    return this.httpClient.post(environment.baseUrl+'/api/login',data);
  }
}