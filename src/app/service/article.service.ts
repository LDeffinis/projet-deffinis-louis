import { Subject } from 'rxjs';
import { Article } from '../../shared/models/article'

export class ArticleService {

  articleSubject = new Subject<Article[]>();

  articles: Article[] = [
    {
      ref: "1",
      titre: "Table",
      prix: 329
    },
    {
      ref: "2",
      titre: "Chaise",
      prix: 54
    },
    {
      ref: "3",
      titre: "Coussin",
      prix: 14
    },
    {
      ref: "4",
      titre: "Tapis",
      prix: 99
    },
    {
      ref: "5",
      titre: "Lampe",
      prix: 119
    },
    {
      ref: "6",
      titre: "Etagère",
      prix: 249
    },
  ];

  emitArticleSubject() {
    this.articleSubject.next(this.articles.slice());
  }

  getArticleByRef(ref: string) {
    const article = this.articles.find(
      (articleObject) => {
        return articleObject.ref === ref;
      }
    );

    return article;
  }

}
